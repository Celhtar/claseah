import json
import logging
from datetime import datetime
from werkzeug.exceptions import Forbidden, NotFound
import werkzeug
from odoo import fields, http, SUPERUSER_ID, tools, _
from odoo.http import request, Controller, route

_logger = logging.getLogger(__name__)

class MyController(Controller):
    @route('/clase_ah', auth='public', website=True)
    def handler(self):
        return request.render('clase_ah.index')