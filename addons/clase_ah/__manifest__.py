# -*- coding: utf-8 -*-
{
    'name': "Clase de Odoo AH",

    'summary': """
        Ejemplo de creacion de modulo Odoo.
        """,

    'description': """
          """,

    'author': "Clase odoo",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': '',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'contacts',
        'website'
        ],
    'demo': [
    ],
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/reports.xml',
        'templates/web.xml'
    ],
    'application': True
    
}