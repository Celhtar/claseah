odoo.define('clase_ah.mascotas', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');
    var core = require('web.core');

    publicWidget.registry.clase_ah_mascotas = publicWidget.Widget.extend({
        selector: '.mascotas',
        template: 'clase_ah.mascotas_list',
        pets: [],
        read_events: {
            'click p': 'on_click_mascota'
        },
        xmlDependencies: ['/clase_ah/static/src/xml/templates.xml'],
        start: async function(){
            var data = await this._rpc({
                'model': 'ah.mascota',
                'method': 'search_read',
                'args': [[],]
            });
            this.pets=data;
            var template = core.qweb.render(
                'clase_ah.mascotas_list',
                {
                    'pets': data
                });
            $('.mascotas').html(template);
        },
        on_click_mascota: function(){
            alert('Click');
        }
    });

});