# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
import base64

class Mascota(models.Model):
    _name = "ah.mascota"   # ah_mascota
    _inherit = ['mail.thread']

    name = fields.Char('Nombre de la mascota', length=50)
    owner = fields.Many2one('res.partner', 'Proprietario')
    image = fields.Image('Imagen')
    pedigree = fields.Binary('Pedigree')
    pedigree_name = fields.Char('Nombre del archivo')


    def print_report(self):
        return self.env.ref('clase_ah.mascotas_report').report_action(self)

    def send_email(self):
        tmpl = self.env.ref('clase_ah.mail_mascotas')
        
        document = base64.b64encode(
            self.env.ref('clase_ah.mascotas_report2')._render_qweb_pdf(self.id)[0]
            )
        attach = self.env['ir.attachment'].create(
            {
                'name': 'Citas de {0}.pdf'.format(self.name),
                'store_fname':'Citas de {0}.pdf'.format(self.name)
                ,
                'datas': document,
                #'datas_fname':  '{0}.xml'.format(self.clave_acceso),
                'res_model': self._name,
                'res_id': self.id,
                'type': 'binary'
            },
        )
        vals = {'attachment_ids': [(4,int(attach.id))]}
        tmpl.send_mail(self.id, force_send=True, email_values=vals)

class Cita(models.Model):
    _name = 'ah.cita'

    cliente_id = fields.Many2one('res.partner', 'Cliente')
    fecha_cita = fields.Datetime('Fecha')
    end_cita = fields.Datetime('Fin de la cita')
    pet_ids = fields.Many2many('ah.mascota', string='Mascota')
    nb_pet = fields.Integer('Numero de mascotas', compute="_compute_num_mascotas") 
    name = fields.Char('Cliente', related="cliente_id.name", readonly=True)

    @api.constrains('pet_ids')
    def _check_valid(self):
        for record in self:
            if record.nb_pet == 0:
                raise ValidationError('Se debe tener minimo una mascota.')   

    @api.depends('pet_ids', 'cliente_id')
    def _compute_num_mascotas(self):
        self.nb_pet = len(self.pet_ids)

    @api.onchange('cliente_id')
    def _onchange_client(self):
        self.pet_ids = [(5, 0, 0)]

class Cliente(models.Model):
    _inherit = 'res.partner'

    pets_ids = fields.One2many('ah.mascota', 'owner', 'Mascotas')

class ParticularReport(models.AbstractModel):
    _name = 'report.clase_ah.mascotas_consultas2' # report.module_name.nombre_report

    
    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('clase_ah.mascotas_consultas2')
        
        mascotas = self.env['ah.mascota'].browse(docids)

        result = []
        for mascota in mascotas:
            citas = self.env['ah.cita'].search([
                ('cliente_id', '=', mascota.owner.id),
                ])
            data = {
                'mascota_name': mascota.name,
                'owner_name': mascota.owner.name,
                'citas': [{
                    'fecha_cita': c.fecha_cita,
                    'end_cita': c.end_cita
                } for c in citas if mascota in c.pet_ids] 
            }
            result.append(data)
        #raise UserError(str(result))
        docargs = {
            'doc_ids': docids,
            'doc_model': 'ah.mascota',
            'docs': self,
            'citas': result
        }
        return docargs

