# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class HistoriaClinica(models.Model):
    _inherit = "hr.employee"

    doctor_id = fields.Many2one('hr.employee', 'Medico asignado', domain=[('is_doctor', '=', True)])
    is_doctor = fields.Boolean('Es medico', default=False)

