# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
import datetime

class CitaMedica(models.Model):
    _name = 'ah.cita.medica'
    employee_id = fields.Many2one('hr.employee', 'Empleado')    
    name = fields.Char('Empleado', related="employee_id.name", readonly=True)
    doctor_id = fields.Many2one('hr.employee', 'Doctor', domain=[('is_doctor', '=', True)])
    inicio_cita = fields.Datetime('Fecha Inicio')
    fin_cita = fields.Datetime('Fecha Fin')
    tipo_cita = fields.Selection(selection=[('iess', 'IESS'), ('local', 'Local')], string='Tipo Cita')
    estado_cita = fields.Selection(selection=[('disponible', 'Disponible'), ('agendada', 'Agendada'), ('finalizada', 'Finalizada')], string='Estado')
    historia_id = fields.Many2one('ah.historial.medico', related="employee_id.historial_id")

    @api.depends('historial_medico', 'employee_id')
    def _compute_historial_medico(self):
        self.historial_medico = HistorialMedico.employee_id

    def generarCitas(self):
        hoy = datetime.datetime.today()
        medicos = self.env['hr.employee'].search([('is_doctor','=',True)])
        for dia in range(7):
            if hoy.weekday() in [0,1,2,3,4]:
                hoy = hoy.replace(hour=8, minute=0, second=0)
                for hora in range (24):
                    if hoy.hour in [8,9,10,11,12,13,14,15,16,17]:                                  
                        for minutos in [0,15,30,45]:
                            if hoy.minute in [0,15,30,45]:
                                hoy_fin =  hoy + datetime.timedelta(minutes=15)
                                cita = self.env['ah.cita.medica']
                                if hoy.hour in [8,9,10,11,12]:
                                    medico = medicos[0]
                                elif hoy.hour in [13,14,15,16]:
                                    medico = medicos[1]
                                cita.create({
                                    'employee_id':None,
                                    'doctor_id':medico.id, 
                                    'inicio_cita':hoy, 
                                    'fin_cita':hoy_fin, 
                                    'tipo_cita':'local',
                                    'estado_cita':'disponible'})                         
                                hoy = hoy_fin
            hoy = hoy + datetime.timedelta(days=1)


class Employee(models.Model):
    _inherit='hr.employee'

    historial_id = fields.Many2one('ah.historial.medico', 'Historial')


class HistorialMedico(models.Model):
    _name = 'ah.historial.medico'

    employee_id = fields.Many2one('hr.employee', 'Empleado')
    antecedentes_id = fields.Many2one('ah.antecedentes.clinicos', 'Antecedentes')
    antecedentes_no_id = fields.Many2one('ah.antecedentes.no.patologicos', 'Antecedentes No Patológicos')
    historia_ids = fields.One2many('ah.historia.clinica', 'historial_id', 'Historia Clínica')

class AntecedentesClinicosFamiliares(models.Model):
    _name = 'ah.antecedentes.clinicos'

    nombre_madre = fields.Char('Nombre Madre', lenght="100")
    is_vivo_madre = fields.Boolean('¿Está viva?', default=True)
    motivo_muerte_madre = fields.Char('Motivo de la muerte', length='300')

    nombre_padre= fields.Char('Nombre Padre', length="100")
    is_vivo_padre = fields.Boolean('¿Está vivo?', default=True)
    motivo_muerte_padre = fields.Char('Motivo de la muerte', length='300')

    is_diabetes =  fields.Boolean('Familiar con Diabetes', default=False)
    who_diabetes = fields.Char('¿Quién?', length='300')

    is_hipertension =  fields.Boolean('Familiar con Hipertensión', default=False)
    who_hipertension = fields.Char('¿Quien?', length='300')

    is_tuberculosis =  fields.Boolean('Familiar con Tuberculosis', default=False)
    who_tuberculosis = fields.Char('¿Quién?', length='300')

    is_cancer =  fields.Boolean('Familiar con Cáncer', default=False)
    who_cancer = fields.Char('¿Quién?', length='300')

    is_otras =  fields.Boolean('Otras enfermedades', default=False)
    who_otras = fields.Char('Detalle', length='300')

    employee_id = fields.Many2one('hr.employee', 'Empleado')

class AntecedentesNoPatologicos(models.Model):
    _name = 'ah.antecedentes.no.patologicos'

    is_alcohol = fields.Boolean('Alcohol', default=False)
    frecuencia_alcohol = fields.Char('Frecuencia Alcohol', lenght='50')

    is_tabaco = fields.Boolean('Tabaco', default=False)
    frecuencia_tabaco = fields.Char('Frecuencia Tabaco', lenght='50')

    is_drogas = fields.Boolean('Drogas', default=False)
    frecuencia_drogas = fields.Char('Frecuencia Drogas', lenght='50')

    employee_id = fields.Many2one('hr.employee', 'Empleado')

class AntcedentesPatologicosPersonales(models.Model):
    _name = 'ah.antecedentes.patologicos.personales'

class HistoriaClinica(models.Model):
    _name = 'ah.historia.clinica'

    historial_id = fields.Many2one('ah.historial.medico', 'Lineas de HC')
    employee_id_ = fields.Many2one('hr.employee', 'Empleado')
    linea_historia = fields.Text('Historia')
    date_update = fields.Datetime('Fecha', default = lambda self: datetime.datetime.now())

